#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include "jsmn.h"

const static char* ros_ip = "127.0.0.1";
const static uint16_t ros_port = 9090;

const static char* f7_ip = "192.168.0.125";
const static uint16_t f7_port = 9087;

const static char* proxy_ip = "192.168.0.88";
const static uint16_t proxy_ros_port = 9089;
const static uint16_t proxy_f7_port = 9088;


struct socket_info {
    int ros_sock;
    int f7_sock;
    struct sockaddr_in ros_addr;
    struct sockaddr_in f7_addr;
};

struct __attribute__((packed)) pkt_pose2d {
    uint8_t opcode;
    uint8_t padding[3];
    float x;
    float y;
    float theta;
};

struct __attribute__((packed)) vec3 {
    float x;
    float y;
    float z;
};

struct __attribute__((packed)) quaternion {
    float x;
    float y;
    float z;
    float w;
};

struct __attribute__((packed)) twist {
    struct vec3 linear;
    struct vec3 angular;
};

struct __attribute__((packed)) pose {
    struct vec3 point;
    struct quaternion orientation;
};

struct __attribute__((packed)) pkt_odom {
    uint8_t opcode;
    uint8_t padding[3];
    struct pose pose;
    struct twist twist;
};

struct __attribute__((packed)) transform {
    struct vec3 translation;
    struct quaternion rotation;
};


struct __attribute__((packed)) pkt_tf {
    uint8_t opcode;
    uint8_t padding[3];
    struct transform transform;
};

int com_init(struct socket_info *sock_info);
int recv_ros(void* p);
int recv_f7(void* p);
float be2le(float val);
int parse_pose2d(struct pkt_pose2d *pose2d, char* json_msg);

int main(void){
    struct socket_info sock_info;


    struct sockaddr_in proxy_ros_addr;
    proxy_ros_addr.sin_family = AF_INET;
    proxy_ros_addr.sin_port = htons(proxy_ros_port);
    proxy_ros_addr.sin_addr.s_addr = inet_addr(proxy_ip);

    struct sockaddr_in proxy_f7_addr;
    proxy_f7_addr.sin_family = AF_INET;
    proxy_f7_addr.sin_port = htons(proxy_f7_port);
    proxy_f7_addr.sin_addr.s_addr = inet_addr(proxy_ip);

    sock_info.ros_addr.sin_family = AF_INET;
    sock_info.ros_addr.sin_port = htons(ros_port);
    sock_info.ros_addr.sin_addr.s_addr = inet_addr(ros_ip);
    
    sock_info.f7_addr.sin_family = AF_INET;
    sock_info.f7_addr.sin_port = htons(f7_port);
    sock_info.f7_addr.sin_addr.s_addr = inet_addr(f7_ip);

    sock_info.f7_sock = socket(AF_INET, SOCK_DGRAM, 0);

    fprintf(stdout, "Starting rosbridge_server proxy...\n");
    if(sock_info.f7_sock < 0){
        perror("Failed to create PC to F7 Socket");
        exit(1);
    }
    bind(sock_info.f7_sock, (struct sockaddr *)&proxy_f7_addr, sizeof(proxy_f7_addr));

    sock_info.ros_sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock_info.ros_sock < 0){
        perror("Failed to create PC to F7 Socket");
        exit(1);
    }
    bind(sock_info.ros_sock, (struct sockaddr *)&proxy_ros_addr, sizeof(proxy_ros_addr));

    pthread_t recv_f7_thread;
    pthread_t recv_ros_thread;
    com_init(&sock_info);
    pthread_create(&recv_f7_thread, NULL, (void *)recv_f7, ((void *)&sock_info));
    pthread_create(&recv_ros_thread, NULL, (void *)recv_ros, ((void *)&sock_info));
    while(1){
    	sleep(10);
    }
    close(sock_info.f7_sock);
    close(sock_info.ros_sock);
    return 0;
}

int com_init(struct socket_info *sock_info){
    const static char* pose2d_sub_msg = "{\"op\": \"subscribe\",\"topic\": \"base_link\",\"type\": \"geometry_msgs/Pose2D\"}";
    const static char* odom_adv_msg = "{\"op\": \"advertise\",\"topic\": \"odom\",\"type\": \"nav_msgs/Odometry\"}";
    const static char* tf_adv_msg = "{\"op\": \"advertise\",\"topic\": \"tf\",\"type\": \"tf/tfMessage\"}";
    printf("%s\n", pose2d_sub_msg);

    while(sendto(sock_info->ros_sock, pose2d_sub_msg, strlen(pose2d_sub_msg), 0, (struct sockaddr *)&sock_info->ros_addr, sizeof(sock_info->ros_addr)) < 0){
        perror("Failed to send pose2d subscribe message");
        sleep(1);
    }
    sleep(1);

    while(sendto(sock_info->ros_sock, odom_adv_msg, strlen(odom_adv_msg), 0, (struct sockaddr *)&sock_info->ros_addr, sizeof(sock_info->ros_addr)) < 0){
        perror("Failed to send odom advertise message");
        sleep(1);
    }
}



#define PROXY_DATA_POSE2D 1
#define PROXY_DATA_ODOM 2
#define PROXY_DATA_TF 3

int recv_f7(void* p){
    fprintf(stdout, "Started F7 Receieve thread\n");
    struct socket_info *sock_info = (struct socket_info *)p;
    static uint8_t recv_buffer[2048];
    while(1){
        int recv_len;
        recv_len = recv(sock_info->f7_sock, recv_buffer, 2048, 0);
        uint8_t json_buf[1024];
        int send_len = 0;
        switch(recv_buffer[0]){
            case PROXY_DATA_ODOM:
                NULL;
                struct pkt_odom *odom = (struct pkt_odom *)recv_buffer;
		float tmp_x = odom->pose.point.x;
                odom->pose.point.x = -odom->pose.point.y;
                odom->pose.point.y = -tmp_x;

                odom->pose.orientation.z = -sin(odom->pose.orientation.x/2);
                odom->pose.orientation.w = cos(odom->pose.orientation.x/2);
                odom->pose.orientation.x = 0;
                odom->pose.orientation.y = 0;
                send_len = sprintf(json_buf,"{\"op\":\"publish\",\"topic\":\"odom\",\"msg\":{\"header\":{\"frame_id\":\"odom\"},\"child_frame_id\":\"horizontal_laser_link\",\"pose\":{\"pose\":{\"position\":{\"x\":%f,\"y\":%f,\"z\":%f},\"orientation\":{\"x\":%f,\"y\":%f,\"z\":%f,\"w\":%f}},\"covariance\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]},\"twist\":{\"twist\":{\"linear\":{\"x\":%f,\"y\":%f,\"z\":%f},\"angular\":{\"x\":%f,\"y\":%f,\"z\":%f}},\"covariance\":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}}}",
                        odom->pose.point.x, odom->pose.point.y, odom->pose.point.z,
                        odom->pose.orientation.x, odom->pose.orientation.y, odom->pose.orientation.z, odom->pose.orientation.w,
                        odom->twist.linear.x, odom->twist.linear.y, odom->twist.linear.z,
                        odom->twist.angular.x, odom->twist.angular.y, odom->twist.angular.z
                       );
                break;
            case PROXY_DATA_TF:
                NULL;
                struct pkt_tf *tf = (struct pkt_tf *)recv_buffer;
                send_len = sprintf(json_buf,"{\"op\":\"publish\",\"topic\":\"tf\",\"msg\":{\"transforms\":[{\"header\":{\"frame_id\":\"Odom\"},\"child_frame_id\":\"base_link\",\"transform\":{\"translation\":{\"x\":%lf,\"y\":%lf,\"z\":%lf},\"rotation\":{\"x\":%lf,\"y\":%lf,\"z\":%lf,\"w\":%lf}}}]}}",
                        tf->transform.translation.x, tf->transform.translation.y, tf->transform.translation.z,
                        tf->transform.rotation.x, tf->transform.rotation.y, tf->transform.rotation.z, tf->transform.rotation.w
                        );
                break;
		default:
		   printf("Unkown:%d\n", recv_buffer[0]);
		   break;
        }
        printf("Send:%d\n",send_len );
        send_len = sendto(sock_info->ros_sock, json_buf, send_len, 0, (struct sockaddr *)&sock_info->ros_addr, sizeof(sock_info->ros_addr));
    }

}

int recv_ros(void* p){
    fprintf(stdout, "Started ROS Receieve thread\n");
    struct socket_info *sock_info = (struct socket_info *)p;
    static uint8_t recv_buffer[2048];
    while(1){
        int recv_len;
        recv_len = recv(sock_info->ros_sock, recv_buffer, 2048, 0);
	for(int i=0;i<recv_len;i++){
	    printf("%c",recv_buffer[i]);
	}
	printf("\n");
        struct pkt_pose2d pose2d;
        parse_pose2d(&pose2d, recv_buffer);
        for(int i=0;i<recv_len;i++){
            recv_buffer[i] = 0;
        }
        //pose2d.x = be2le(pose2d.x);
        //pose2d.y = be2le(pose2d.y);
        //pose2d.theta = be2le(pose2d.theta);
        pose2d.opcode = PROXY_DATA_POSE2D;
        sendto(sock_info->f7_sock, &pose2d, sizeof(struct pkt_pose2d), 0, (struct sockaddr *)&sock_info->f7_addr, sizeof(sock_info->f7_addr));
    }
}

int parse_pose2d(struct pkt_pose2d *pose2d, char* json_msg){
    //static const char* json_msg = "{\"topic\": \"base_link\", \"msg\": {\"y\": 5.7, \"x\": -3.25, \"theta\": 2.89}, \"op\": \"publish\"}";
    jsmn_parser p;
    jsmntok_t tokens[20];
    jsmn_init(&p);
    int res  = jsmn_parse(&p, json_msg, strlen(json_msg),tokens, 20);
    if(res < 0){
        fprintf(stderr, "Failed to parse JSON:%s\n", json_msg);
        return 0;
    }
    if(tokens[0].type != JSMN_OBJECT){
        fprintf(stderr, "Not Matched JSMN type at tokens[0]\n");
        return 0;
    }

    //topic
    if(tokens[1].type != JSMN_STRING){
        fprintf(stderr, "Not Matched JSMN type at tokens[1]\n");
        return 0;
    }
    if(strncmp("topic", json_msg+tokens[1].start, tokens[1].end - tokens[1].start) != 0){
        fprintf(stderr, "Not Matched parsed value:topic\n");
        return 0;
    }
    if(tokens[2].type != JSMN_STRING){
        fprintf(stderr, "Not Matched JSMN type at tokens[2]\n");
        return 0;
    }
    if(strncmp("base_link", json_msg+tokens[2].start, tokens[2].end - tokens[2].start) != 0){
        fprintf(stderr, "Not Matched parsed value:base_link\n");
        return 0;
    }

    //msg
    if(tokens[3].type != JSMN_STRING){
        fprintf(stderr, "Not Matched JSMN type at tokens[3]\n");
        return 0;
    }
    if(strncmp("msg", json_msg+tokens[3].start, tokens[3].end - tokens[3].start) != 0){
        fprintf(stderr, "Not Matched parsed value:msg\n");
        return 0;
    }
    if(tokens[4].type != JSMN_OBJECT){
        fprintf(stderr, "Not Matched JSMN type at tokens[4]\n");
        return 0;
    }
    if(tokens[5].type != JSMN_STRING){
        fprintf(stderr, "Not Matched JSMN type at tokens[5]\n");
        return 0;
    }
    if(tokens[6].type != JSMN_PRIMITIVE){
        fprintf(stderr, "Not Matched JSMN type at tokens[6]\n");
        return 0;
    }
    if(tokens[7].type != JSMN_STRING){
        fprintf(stderr, "Not Matched JSMN type at tokens[7]\n");
        return 0;
    }
    if(tokens[8].type != JSMN_PRIMITIVE){
        fprintf(stderr, "Not Matched JSMN type at tokens[8]\n");
        return 0;
    }
    if(tokens[9].type != JSMN_STRING){
        fprintf(stderr, "Not Matched JSMN type at tokens[9]\n");
        return 0;
    }
    if(tokens[10].type != JSMN_PRIMITIVE){
        fprintf(stderr, "Not Matched JSMN type at tokens[10]\n");
        return 0;
    }
    char col1_tmp[30] = {0};
    char col2_tmp[30] = {0};
    char col3_tmp[30] = {0};
    strncpy(col1_tmp, json_msg+tokens[6].start, tokens[6].end - tokens[6].start);
    strncpy(col2_tmp, json_msg+tokens[8].start, tokens[8].end - tokens[8].start);
    strncpy(col3_tmp, json_msg+tokens[10].start, tokens[10].end - tokens[10].start);
    float col1 = atof(col1_tmp);
    float col2 = atof(col2_tmp);
    float col3 = atof(col3_tmp);

    if(json_msg[tokens[5].start] == 'x'){
        pose2d->x = col1;
    }else if(json_msg[tokens[5].start] == 'y'){
        pose2d->y = col1;
    }else if(json_msg[tokens[5].start] == 't'){
        pose2d->theta = col1;
    }

    if(json_msg[tokens[7].start] == 'x'){
        pose2d->x = col2;
    }else if(json_msg[tokens[7].start] == 'y'){
        pose2d->y = col2;
    }else if(json_msg[tokens[7].start] == 't'){
        pose2d->theta = col2;
    }

    if(json_msg[tokens[9].start] == 'x'){
        pose2d->x = col3;
    }else if(json_msg[tokens[9].start] == 'y'){
        pose2d->y = col3;
    }else if(json_msg[tokens[9].start] == 't'){
        pose2d->theta = col3;
    }
}

union float_uint8 {
    uint8_t u8_val[4];
    float f_val;
};
float be2le(float val){
    union float_uint8 be_val;
    union float_uint8 le_val;
    be_val.f_val = val;

    le_val.u8_val[0] = be_val.u8_val[3];
    le_val.u8_val[1] = be_val.u8_val[2];
    le_val.u8_val[2] = be_val.u8_val[1];
    le_val.u8_val[3] = be_val.u8_val[0];

    return le_val.f_val;
}
